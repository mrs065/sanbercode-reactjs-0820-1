// soal 1
var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

var a = kataPertama;
var b = kataKedua[0].toUpperCase() + kataKedua.slice(1);
var c = kataKetiga;
var d = kataKeempat.toUpperCase();

console.log(a + " " + b + " " + c + " " + d);

// soal 2
var kataPertama = "1";
var kataKedua = "2";
var kataKetiga = "4";
var kataKeempat = "5";

console.log(parseInt(kataPertama) + parseInt(kataKedua) + parseInt(kataKetiga) + parseInt(kataKeempat));

// soal 3
var kalimat = 'wah javascript itu keren sekali'; 

var kataPertama = kalimat.substring(0, 3); 
var kataKedua = kalimat.substring(4, 14); // do your own! 
var kataKetiga = kalimat.substr(15, 3); // do your own! 
var kataKeempat = kalimat.substr(19, 5); // do your own! 
var kataKelima = kalimat.substring(25, 31); // do your own! 

console.log('Kata Pertama: ' + kataPertama); 
console.log('Kata Kedua: ' + kataKedua); 
console.log('Kata Ketiga: ' + kataKetiga); 
console.log('Kata Keempat: ' + kataKeempat); 
console.log('Kata Kelima: ' + kataKelima);

// soal 4
var nilai;
nilai = 95;
if(nilai >= 80) {
    console.log('A');
} else if(nilai >= 70 && nilai<80) {
    console.log('B');
} else if(nilai >= 60 && nilai<70) {
    console.log('C');
} else if(nilai >= 50 && nilai<60) {
    console.log('D');
} else {
    console.log('E');
}

// soal 5
var tanggal = 10;
var bulan = 2;
var tahun = 1995;
var bulanstr;
switch(bulan) {
    case 1: {bulanstr='Januari'; break;}
    case 2: {bulanstr='Februari'; break;}
    case 3: {bulanstr='Maret'; break;}
    case 4: {bulanstr='April'; break;}
    case 5: {bulanstr='Mei'; break;}
    case 6: {bulanstr='Juni'; break;}
    case 7: {bulanstr='Juli'; break;}
    case 8: {bulanstr='Agustus'; break;}
    case 9: {bulanstr='September'; break;}
    case 10: {bulanstr='Oktober'; break;}
    case 11: {bulanstr='November'; break;}
    case 12: {bulanstr='Desember'; break;}
};
console.log(tanggal + " " + bulanstr + " " + tahun);