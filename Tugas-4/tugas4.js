// soal 1
console.log('LOOPING PERTAMA');
var i = 2;
while(i < 21){
    console.log(i + ' - I love coding');
    i+=2;
}
console.log('LOOPING KEDUA');
i-=2;
while(i > 1){
    console.log(i + ' - I will become a frontend developer');
    i-=2;
}

console.log('----------------------------');

// soal 2
for(var j = 1; j < 21; j++){
    if(j%3==0 && j%2==1){
        console.log(j + ' - I Love Coding');
    } else if(j%2==1){
        console.log(j + ' - Santai');
    } else if(j%2==0){
        console.log(j + ' - Berkualitas');
    }
};

console.log('----------------------------');

// soal 3
var x = '#'
console.log(x);
for(var k = 1; k < 7; k++){
    x = x.concat('#');
    console.log(x);
}

console.log('----------------------------');

// soal 4
var kalimat="saya sangat senang belajar javascript"
var pecah=kalimat.split(" ");
console.log(pecah);

console.log('----------------------------');

// soal 5
var daftarBuah = ["2. Apel", "5. Jeruk", "3. Anggur", "4. Semangka", "1. Mangga"];
var urut = daftarBuah.sort();
var panjang = daftarBuah.length;
for(var z = 1; z <= panjang; z++){
    console.log(urut.shift());
}