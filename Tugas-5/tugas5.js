// soal 1
console.log("SOAL 1");
function halo(){
    return "Halo Sanbers!";
}
console.log(halo());

console.log("--------------------");

// soal 2
console.log("SOAL 2");
function kalikan(num1, num2){
    return num1 * num2;
}

var num1 = 12;
var num2 = 4;
 
var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

console.log("--------------------");

// soal 3
console.log("SOAL 3");
function introduce(name, age, address, hobby){
    return "Nama saya " + name + ", umur saya " + age + " tahun, alamat saya di " + address + ", dan saya punya hobby yaitu " + hobby + "!";
}

var name = "John";
var age = 30;
var address = "Jalan belum jadi";
var hobby = "Gaming";
 
var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);

console.log("--------------------");

// soal 4
console.log("SOAL 4");
var arrayDaftarPeserta = ["Asep", "laki-laki", "baca buku" , 1992];
var daftarPeserta = {}
daftarPeserta.nama = arrayDaftarPeserta[0];
daftarPeserta.jenisKelamin = arrayDaftarPeserta[1];
daftarPeserta.hobi = arrayDaftarPeserta[2];
daftarPeserta.tahunLahir = arrayDaftarPeserta[3];
console.log(daftarPeserta);

console.log("--------------------");

// soal 5
console.log("SOAL 5");
var data = [
    {
        no : 1,
        nama : "strawberry",
        warna : "merah",
        adaBijinya : 0,
        harga : 9000
    },
    {
        no : 2,
        nama : "jeruk",
        warna : "oranye",
        adaBijinya : 1,
        harga : 8000
    },
    {
        no : 3,
        nama : "semangka",
        warna : "hijau & merah",
        adaBijinya : 1,
        harga : 10000
    },
    {
        no : 4,
        nama : "pisang",
        warna : "kuning",
        adaBijinya : 0,
        harga : 5000
    }
];
console.log(data[0]);

console.log("--------------------");

// soal 6
console.log("SOAL 6");
var dataFilm = []
function tambahFilm(nm, drs, gnre, thn){
    dataFilm.push({nama : nm, durasi : drs, genre : gnre, tahun : thn});

    return dataFilm;
}
tambahFilm("Onward", "103 menit", "komedi", 2020);
tambahFilm("Bloodshot", "1 jam 40 menit", "action", 2020);
console.log(dataFilm);