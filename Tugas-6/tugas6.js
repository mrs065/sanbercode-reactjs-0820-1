// soal 1
console.log('SOAL 1');
const phi = 22/7;
const luasLingk = (jari) => {
    return phi*jari*jari;
}

const kelLingk = (jari) => {
    return phi * 2 * jari;
}

console.log(luasLingk(7));
console.log(kelLingk(7));

console.log('----------------------------');

// soal 2
console.log('SOAL 2');
let kalimat = "";
const kataPertama = "saya";
const kataKedua = "adalah";
const kataKetiga = "seorang";
const kataKeempat = "frontend";
const kataKelima = "developer";

// var sambungKata = (kataPertama, kataKedua, kataKetiga, kataKeempat, kataKelima) => {
//     return kalimat = `${kataPertama} ${kataKedua} ${kataKetiga} ${kataKeempat} ${kataKelima}`;
// }

kalimat = `${kataPertama} ${kataKedua} ${kataKetiga} ${kataKeempat} ${kataKelima}`;
console.log(kalimat);

console.log('----------------------------');

// soal 3
console.log('SOAL 3');
const newFunction = (firstName, lastName) => {
    firstName,
    lastName,
    fullName = () => console.log(`${firstName} ${lastName}`)
    
}
newFunction('William', 'Imoh');
console.log(fullName());

console.log('----------------------------');

// soal 4
console.log('SOAL 4');
const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
}
const {firstName, lastName, destination, occupation} = newObject;
console.log(firstName, lastName, destination, occupation);

console.log('----------------------------');

// soal 5
console.log('SOAL 5');
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
//const combined = west.concat(east) -> normal js
const combined = [...west, ...east]
console.log(combined);