// di index.js
var readBooks = require('./callback.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000},
    {name: 'komik', timeSpent: 1000}
]
 
// Tulis code untuk memanggil function readBooks di sini
let i = 1;

function antriBuku(sisaWaktu, book){
    readBooks(sisaWaktu, book, function(sisaWaktu){
        if(i<=3){
            antriBuku(sisaWaktu, books[i]);
            i++;
        }
    })
}

antriBuku(10000,books[0])