var readBooksPromise = require('./promise.js')
 
var books = [
    {name: 'LOTR', timeSpent: 3000}, 
    {name: 'Fidas', timeSpent: 2000}, 
    {name: 'Kalkulus', timeSpent: 4000}
]
 
// Lanjutkan code untuk menjalankan function readBooksPromise
let i = 1;

function antrianBuku(sisaWaktu, book){
    if(i<3){
        readBooksPromise(sisaWaktu, book)
        .then(function(sisaWaktu){
            antrianBuku(sisaWaktu, books[i])
            i++;
        })
        .catch(function(error){
            console.log(error.message);
        })
    }
}

antrianBuku(10000, books[0])